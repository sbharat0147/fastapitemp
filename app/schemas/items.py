from pydantic import BaseModel, Field

class ItemBase(BaseModel):
    name: str
    description: str | None = Field(default=None, description="Optional description of the item")

class ItemCreate(ItemBase):
    pass  # For now, inherits all fields from ItemBase

class Item(ItemBase):
    id: int

    class Config:
        orm_mode = True  # Enables compatibility with SQLAlchemy models
