from sqlalchemy.orm import Session
from app.models.formation_master import FormationMaster, FormationType, FormationLevel
from app.schemas.formation import (
    FormationMasterCreate, FormationMaster, FormationTypeCreate, FormationType,
    FormationLevelCreate, FormationLevelBase
)

# FormationMaster CRUD operations

def create_formation_master(db: Session, formation: FormationMasterCreate):
    db_formation = FormationMaster(**formation.dict())
    db.add(db_formation)
    db.commit()
    db.refresh(db_formation)
    return db_formation

def get_formation_master(db: Session, formation_id: int):
    return db.query(FormationMaster).filter(FormationMaster.id == formation_id).first()

def get_all_formation_masters(db: Session, skip: int = 0, limit: int = 10):
    return db.query(FormationMaster).offset(skip).limit(limit).all()

def update_formation_master(db: Session, formation_id: int, formation: FormationMasterCreate):
    db_formation = db.query(FormationMaster).filter(FormationMaster.id == formation_id).first()
    if db_formation:
        for field, value in formation.dict().items():
            setattr(db_formation, field, value)
        db.commit()
        db.refresh(db_formation)
    return db_formation

def delete_formation_master(db: Session, formation_id: int):
    db_formation = db.query(FormationMaster).filter(FormationMaster.id == formation_id).first()
    if db_formation:
        db.delete(db_formation)
        db.commit()
    return db_formation

# FormationType CRUD operations

def create_formation_type(db: Session, formation_type: FormationTypeCreate):
    db_formation_type = FormationType(**formation_type.dict())
    db.add(db_formation_type)
    db.commit()
    db.refresh(db_formation_type)
    return db_formation_type

def get_formation_type(db: Session, formation_type_id: int):
    return db.query(FormationType).filter(FormationType.id == formation_type_id).first()

def get_all_formation_types(db: Session, skip: int = 0, limit: int = 10):
    return db.query(FormationType).offset(skip).limit(limit).all()

def update_formation_type(db: Session, formation_type_id: int, formation_type: FormationTypeCreate):
    db_formation_type = db.query(FormationType).filter(FormationType.id == formation_type_id).first()
    if db_formation_type:
        for field, value in formation_type.dict().items():
            setattr(db_formation_type, field, value)
        db.commit()
        db.refresh(db_formation_type)
    return db_formation_type

def delete_formation_type(db: Session, formation_type_id: int):
    db_formation_type = db.query(FormationType).filter(FormationType.id == formation_type_id).first()
    if db_formation_type:
        db.delete(db_formation_type)
        db.commit()
    return db_formation_type

# FormationLevel CRUD operations

def create_formation_level(db: Session, formation_level: FormationLevelCreate):
    db_formation_level = FormationLevel(**formation_level.dict())
    db.add(db_formation_level)
    db.commit()
    db.refresh(db_formation_level)
    return db_formation_level

def get_formation_level(db: Session, formation_level_id: int):
    return db.query(FormationLevel).filter(FormationLevel.id == formation_level_id).first()

def get_all_formation_levels(db: Session, skip: int = 0, limit: int = 10):
    return db.query(FormationLevel).offset(skip).limit(limit).all()

def update_formation_level(db: Session, formation_level_id: int, formation_level: FormationLevelCreate):
    db_formation_level = db.query(FormationLevel).filter(FormationLevel.id == formation_level_id).first()
    if db_formation_level:
        for field, value in formation_level.dict().items():
            setattr(db_formation_level, field, value)
        db.commit()
        db.refresh(db_formation_level)
    return db_formation_level

def delete_formation_level(db: Session, formation_level_id: int):
    db_formation_level = db.query(FormationLevel).filter(FormationLevel.id == formation_level_id).first()
    if db_formation_level:
        db.delete(db_formation_level)
        db.commit()
    return db_formation_level
