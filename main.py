from fastapi import FastAPI
from app.db.connection import engine, Base  # Assuming SQLAlchemy
from app.routes import items, formation  # Example routes

Base.metadata.create_all(bind=engine)  # Create DB tables (if needed)

app = FastAPI()
app.include_router(items.router, prefix="/api")
app.include_router(formation.router, prefix="/api")
# ... include more routers as you add them



# from typing import Union
#
# from fastapi import FastAPI
#
# app = FastAPI()
#
#
# @app.get("/")
# def read_root():
#     return {"Hello": "World"}
#
#
# @app.get("/items/{item_id}")
# def read_item(item_id: int, q: Union[str, None] = None):
#     return {"item_id": item_id, "q": q}