from sqlalchemy.orm import Session
from app.models.items import Item as DBItem
from app.schemas.items import ItemCreate

def create_item(db: Session, item: ItemCreate):
    db_item = DBItem(**item.dict())
    db.add(db_item)
    db.commit()
    db.refresh(db_item)
    return db_item

def get_item(db: Session, item_id: int):
    return db.query(DBItem).filter(DBItem.id == item_id).first()

def get_items(db: Session, skip: int = 0, limit: int = 10):
    return db.query(DBItem).offset(skip).limit(limit).all()

def update_item(db: Session, item_id: int, item: ItemCreate):
    db_item = db.query(DBItem).filter(DBItem.id == item_id).first()
    for key, value in item.dict().items():
        setattr(db_item, key, value)
    db.commit()
    db.refresh(db_item)
    return db_item

def delete_item(db: Session, item_id: int):
    db_item = db.query(DBItem).filter(DBItem.id == item_id).first()
    db.delete(db_item)
    db.commit()
    return db_item
