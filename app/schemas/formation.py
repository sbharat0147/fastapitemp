from typing import Optional
from pydantic import BaseModel
from datetime import datetime


class FormationLevelBase(BaseModel):
    name: str
    description: str
    created_by: Optional[int] = None
    updated_by: Optional[int] = None
    deleted_by: Optional[int] = None
    is_active: Optional[bool] = True
    status: str


class FormationLevelCreate(FormationLevelBase):
    created_at: datetime
    updated_at: Optional[datetime]
    deleted_at: Optional[datetime]

    class Config:
        orm_mode = True


class FormationLevelShow(FormationLevelBase):
    id: int


class FormationTypeBase(BaseModel):
    name: str
    description: str
    is_active: Optional[bool] = True
    status: str


class FormationTypeCreate(FormationTypeBase):
    pass


class FormationType(FormationTypeBase):
    id: int
    created_at: datetime
    updated_at: Optional[datetime]
    deleted_at: Optional[datetime]

    class Config:
        orm_mode = True


class FormationMasterBase(BaseModel):
    name: str
    description: str
    formation_level_id: int
    formation_type_id: int
    created_by: int
    updated_by: int
    deleted_by: Optional[int]
    location: str
    is_active: Optional[bool] = True
    status: str


class FormationMasterCreate(FormationMasterBase):
    pass


class FormationMaster(FormationMasterBase):
    id: int
    created_at: datetime
    updated_at: Optional[datetime]
    deleted_at: Optional[datetime]
    formation_level: FormationLevelBase
    formation_type: FormationType

    class Config:
        orm_mode = True
