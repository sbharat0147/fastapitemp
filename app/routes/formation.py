from typing import List
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from app.db.connection import get_db
from app.crud.formation import (
    create_formation_master, get_formation_master, get_all_formation_masters,
    update_formation_master, delete_formation_master,
    create_formation_type, get_formation_type, get_all_formation_types,
    update_formation_type, delete_formation_type,
    create_formation_level, get_formation_level, get_all_formation_levels,
    update_formation_level, delete_formation_level,
)
from app.schemas.formation import (
    FormationMasterCreate, FormationMaster, FormationTypeCreate, FormationType,
    FormationLevelCreate, FormationLevelBase, FormationLevelShow
)

router = APIRouter()

# FormationMaster routes

@router.post("/formation_master/", response_model=FormationMaster)
def create_formation_master_api(formation: FormationMasterCreate, db: Session = Depends(get_db)):
    return create_formation_master(db, formation)

@router.get("/formation_master/{formation_id}", response_model=FormationMaster)
def read_formation_master_api(formation_id: int, db: Session = Depends(get_db)):
    db_formation = get_formation_master(db, formation_id)
    if db_formation is None:
        raise HTTPException(status_code=404, detail="Formation Master not found")
    return db_formation

@router.get("/formation_master/", response_model=List[FormationMaster])
def read_all_formation_masters_api(skip: int = 0, limit: int = 10, db: Session = Depends(get_db)):
    return get_all_formation_masters(db, skip=skip, limit=limit)

@router.put("/formation_master/{formation_id}", response_model=FormationMaster)
def update_formation_master_api(formation_id: int, formation: FormationMasterCreate, db: Session = Depends(get_db)):
    db_formation = update_formation_master(db, formation_id, formation)
    if db_formation is None:
        raise HTTPException(status_code=404, detail="Formation Master not found")
    return db_formation

@router.delete("/formation_master/{formation_id}", response_model=FormationMaster)
def delete_formation_master_api(formation_id: int, db: Session = Depends(get_db)):
    db_formation = delete_formation_master(db, formation_id)
    if db_formation is None:
        raise HTTPException(status_code=404, detail="Formation Master not found")
    return db_formation

# FormationType routes

@router.post("/formation_type/", response_model=FormationType)
def create_formation_type_api(formation_type: FormationTypeCreate, db: Session = Depends(get_db)):
    return create_formation_type(db, formation_type)

@router.get("/formation_type/{formation_type_id}", response_model=FormationType)
def read_formation_type_api(formation_type_id: int, db: Session = Depends(get_db)):
    db_formation_type = get_formation_type(db, formation_type_id)
    if db_formation_type is None:
        raise HTTPException(status_code=404, detail="Formation Type not found")
    return db_formation_type

@router.get("/formation_type/", response_model=List[FormationType])
def read_all_formation_types_api(skip: int = 0, limit: int = 10, db: Session = Depends(get_db)):
    return get_all_formation_types(db, skip=skip, limit=limit)

@router.put("/formation_type/{formation_type_id}", response_model=FormationType)
def update_formation_type_api(formation_type_id: int, formation_type: FormationTypeCreate, db: Session = Depends(get_db)):
    db_formation_type = update_formation_type(db, formation_type_id, formation_type)
    if db_formation_type is None:
        raise HTTPException(status_code=404, detail="Formation Type not found")
    return db_formation_type

@router.delete("/formation_type/{formation_type_id}", response_model=FormationType)
def delete_formation_type_api(formation_type_id: int, db: Session = Depends(get_db)):
    db_formation_type = delete_formation_type(db, formation_type_id)
    if db_formation_type is None:
        raise HTTPException(status_code=404, detail="Formation Type not found")
    return db_formation_type

# FormationLevel routes

@router.post("/formation_level/", response_model=FormationLevelCreate)
def create_formation_level_api(formation_level: FormationLevelCreate, db: Session = Depends(get_db)):
    return create_formation_level(db, formation_level)

@router.get("/formation_level/{formation_level_id}", response_model=FormationLevelShow)
def read_formation_level_api(formation_level_id: int, db: Session = Depends(get_db)):
    db_formation_level = get_formation_level(db, formation_level_id)
    if db_formation_level is None:
        raise HTTPException(status_code=404, detail="Formation Level not found")
    return db_formation_level

@router.get("/formation_level/", response_model=List[FormationLevelShow])
def read_all_formation_levels_api(skip: int = 0, limit: int = 10, db: Session = Depends(get_db)):
    return get_all_formation_levels(db, skip=skip, limit=limit)

@router.put("/formation_level/{formation_level_id}", response_model=FormationLevelBase)
def update_formation_level_api(formation_level_id: int, formation_level: FormationLevelCreate, db: Session = Depends(get_db)):
    db_formation_level = update_formation_level(db, formation_level_id, formation_level)
    if db_formation_level is None:
        raise HTTPException(status_code=404, detail="Formation Level not found")
    return db_formation_level

@router.delete("/formation_level/{formation_level_id}", response_model=FormationLevelBase)
def delete_formation_level_api(formation_level_id: int, db: Session = Depends(get_db)):
    db_formation_level = delete_formation_level(db, formation_level_id)
    if db_formation_level is None:
        raise HTTPException(status_code=404, detail="Formation Level not found")
    return db_formation_level
