# from fastapi import APIRouter, Depends
# from app.db.connection import get_db  # Assuming a database session
# from app.schemas.items import Item, ItemCreate  # Replace with your schemas
# from app.auth.keyclock import get_current_user  # Example Keycloak auth
# from typing import List
#
# router = APIRouter(prefix="/items", tags=["items"])
#
# # Placeholder - you'll implement CRUD operations here
# @router.get("/", response_model=List[Item])
# async def get_items(db=Depends(get_db), user=Depends(get_current_user)):
#     items = db.query(Item).all()
#     return items


from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from app.db.connection import get_db
from app.schemas.items import Item, ItemCreate
from app.crud.items import create_item, get_item, get_items, update_item, delete_item

router = APIRouter()

@router.post("/items/", response_model=Item)
def create_item_api(item: ItemCreate, db: Session = Depends(get_db)):
    return create_item(db, item)

@router.get("/items/{item_id}", response_model=Item)
def read_item(item_id: int, db: Session = Depends(get_db)):
    db_item = get_item(db, item_id)
    if db_item is None:
        raise HTTPException(status_code=404, detail="Item not found")
    return db_item

@router.get("/items/", response_model=list[Item])
def read_items(skip: int = 0, limit: int = 10, db: Session = Depends(get_db)):
    return get_items(db, skip=skip, limit=limit)

@router.put("/items/{item_id}", response_model=Item)
def update_item_api(item_id: int, item: ItemCreate, db: Session = Depends(get_db)):
    db_item = update_item(db, item_id, item)
    if db_item is None:
        raise HTTPException(status_code=404, detail="Item not found")
    return db_item

@router.delete("/items/{item_id}", response_model=Item)
def delete_item_api(item_id: int, db: Session = Depends(get_db)):
    db_item = delete_item(db, item_id)
    if db_item is None:
        raise HTTPException(status_code=404, detail="Item not found")
    return db_item
