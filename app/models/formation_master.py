from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, Boolean
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func
from app.db.connection import Base

class FormationMaster(Base):
    __tablename__ = "formation_master"

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    name = Column(String, index=True)
    description = Column(String)
    formation_level_id = Column(Integer, ForeignKey("formation_level.id"))
    formation_type_id = Column(Integer, ForeignKey("formation_type.id"))
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    created_by = Column(Integer)
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())
    updated_by = Column(Integer)
    deleted_at = Column(DateTime(timezone=True))
    deleted_by = Column(Integer)
    location = Column(String)
    is_active = Column(Boolean, default=True)
    status = Column(String)

    # Relationships
    formation_level = relationship("FormationLevel", back_populates="formation_masters")
    formation_type = relationship("FormationType", back_populates="formation_masters")

class FormationType(Base):
    __tablename__ = "formation_type"

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    name = Column(String, index=True)
    description = Column(String)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    created_by = Column(Integer)
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())
    updated_by = Column(Integer)
    deleted_at = Column(DateTime(timezone=True))
    deleted_by = Column(Integer)
    is_active = Column(Boolean, default=True)
    status = Column(String)

    # Relationships
    formation_masters = relationship("FormationMaster", back_populates="formation_type")

class FormationLevel(Base):
    __tablename__ = "formation_level"

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    name = Column(String, index=True)
    description = Column(String)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    created_by = Column(Integer)
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())
    updated_by = Column(Integer)
    deleted_at = Column(DateTime(timezone=True))
    deleted_by = Column(Integer)
    is_active = Column(Boolean, default=True)
    status = Column(String)

    # Relationships
    formation_masters = relationship("FormationMaster", back_populates="formation_level")
